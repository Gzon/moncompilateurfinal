# Mon Compilateur final

## Introduction


J'ai pris pour partir d'une base propre le TP du framagit de Mr Jourlin ([lien framagit](https://framagit.org/jourlin/cericompiler)).


J'y ai implémenté la fonction ForStatement, en ajoutant la possibilité d'avoir

* un i qui s'incrémente (avec un pas de 1) avec le mot clé ***TO*** ou se décrémente (avec un pas de 1) avec le mot clé ***DOWNTO*** (qui été déjà rajouté dans le fichier *tokeniser.l*).
* un pas différent de 1 en ajoutant le keyword ***STEP*** dans le fichier *tokeniser.l*).

## Grammaire

* ***Program** := [VarDeclarationPart] StatementPart*
* ***VarDeclarationPart** := "VAR" VarDeclaration {";" VarDeclaration} "."*
* ***VarDeclaration** := Identifier {"," Identifier} ":" Type*
* ***StatementPart** := Statement {";" Statement} "."*
-------
* ***Statement** := AssignementStatement | DisplayStatement | ForStatement | BlockStatement | IfStatement | WhileStatement*
* ***AssignementStatement** := Identifier ":=" Expression*
* ***DisplayStatement** := "DISPLAY" Expression*
* ***ForStatement** := "FOR" AssignementStatement "TO"|"DOWNTO" Expression ["STEP" Number] "DO" Statement*
* ***BlockStatement** := "BEGIN" Statement {";" Statement} "END"*
* ***IfStatement** := "IF" Expression "THEN" Statement ["ELSE" Statement]*
* ***WhileStatement** := "WHILE" Expression "DO" Statement*
-------
* ***Expression** := SimpleExpression [RelationalOperator SimpleExpression]*
* ***SimpleExpression** := Term {AdditiveOperator Term}*
* ***Term** := Factor {MultiplicativeOperator Factor}*
* ***Factor** := Number | Identifier | "(" Expression ")"| "!" Factor*
* ***Identifier** := Alpha{Alpha|Digit}*
* ***Number** := Digit{Digit}*
-------
* ***AdditiveOperator** := "+" | "-" | "||"*
* ***MultiplicativeOperator*** *:= "***" | "/" | "%" | "&&"*
* ***RelationalOperator** := "==" | "!=" | "<" | ">" | "<=" | ">="*
* ***Digit** := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"*
* ***Alpha** := "a"|...|"z"|"A"|...|"Z"*

## Fonction ForStatement

Cette fonction permet dans le fichier test.p d'écrire une boucle for de la forme :

*"FOR" AssignementStatement "TO/DOWNTO" Expression ["STEP" Number] "DO" Statement*

avec :

* ***AssignementStatement*** une déclaration de variable ENTIÈRE (ex : *i := 0*).
     * ATTENTION, si vous déclarer par exemple un DOUBLE i := 1.2, vous aurez une erreur de compilation.
* ***Expression*** : une expression de comparaison ou un entier (*10* ~~ou *i ==10*~~)
     * (si vous entrez autre chose qu'un entier ~~ou qu'une expression de comparaison~~, le compilateur affichera une erreur)
     * Par exemple : *FOR i := 0 TO 10.2* affichera une erreur.
* ***Number*** : Un nombre entier
* ***Statement*** : une ou plusieurs instruction(s)
    * *ex : a := a + b*

Au début de mon travail sur ce code, j'ai voulu autoriser l'utilisateur à écrire une ***Expression*** de type *booléen* (c'est à dire une expression de comparaison).
Cependant je me suis rendu compte que beaucoup de problèmes se créaient si l'utilisateur n'était pas vigilant.
En effet voici deux exemples similaires mais qui révèlent déjà deux problèmes différents :
### Exemple 1

> Admettons que dans le fichier *test.p* l'utilisateur écrive cette
> boucle ***FOR i:=1 TO i < 4 STEP 2 DO*** alors pour sortir de la
> boucle il faut que le résultat de **Expression** soit **faux**
> (*expression booléenne = 0*) pour passer à la suite, donc il faut que
> j'écrive dans mon code c++ :
>```cpp
>Expression();
>cout << "\tpop %rax"<<endl;
>cout << "\tcmpq $0, %rax"<<endl;
>cout << "\tje Suite" << newTag << endl;
>```
> Or dans ce cas si l'utilisateur rentre ***FOR i:=1 TO i > 4 STEP 2
> DO*** la boucle for est inutile car le résultat de l'expression
> booléenne sera dès le début fausse donc il ne fera jamais la ou les
> instruction(s) du Statement.

### Exemple 2 (cas inverse de l'Exemple 1)

> Admettons que dans le fichier *test.p* l'utilisateur écrive cette
> boucle ***FOR i:=1 TO i >4 STEP 2 DO*** alors pour sortir de la boucle
> il faut que le résultat de **Expression** soit **vrai** (*expression
> booléenne != 0*) pour passer à la suite, donc il faut que j'écrive
> dans mon code c++
>```cpp
>Expression();
>cout << "\tpop %rax"<<endl;
>cout << "\tcmpq $0, %rax"<<endl;
>cout << "\tjne Suite" << newTag << endl;
>```
> Or dans ce cas si l'utilisateur écris ***FOR i:=1 TO i <4 STEP 2 DO***
> la boucle for est inutile car le résultat de l'expression booléenne
> sera dès le début vraie donc il ne fera jamais la ou les
> instruction(s) du Statement.

C'est donc pour cela que par la suite j'ai décidé de forcer l'utilisateur à écrire une ***Expression*** de type *entier*, renvoyant une erreur le cas échéant.
J'ai tout de même laisser le code traitant le cas d'une expression booléenne en commentaire avec les balise **/*** et ***/** (pour les différencier de mes commentaires de code qui ont la balise **//**.

## Fichiers Test

Voici 9 fichiers test différents, testant les différentes erreurs possibles (les 4 premiers test sont valides).
Pour tester ces fichier il faut les tester un par un en les renommant *test.p* et ensuite exécuter la commande *make*.


  **test1.p :**

  > Test fonctionnel avec un TO sans modification du STEP

  **test2.p :**

  > Test fonctionnel avec un TO avec modification du STEP

  **test3.p :**

  > Test fonctionnel avec un DOWNTO sans modification du STEP

  **test4.p :**

  > Test fonctionnel avec un DOWNTO avec modification du STEP

  **test5.p :**

  > Test non fonctionnel : boucle FOR i := 1.2 or i doit être de type entier.

  **test6.p :**

  > Test non fonctionnel : Expression doit être une expression de type entier (et non pas de la forme d'une expression de type flottant).

  **test7.p :**

  > Test non fonctionnel : Expression doit être une expression de type entier (et non pas de la forme d'une expression booléenne).

  **test8.p :**

  > Test non fonctionnel : Le STEP doit être de type Nombre (et ne peut pas être une expression, ou une variable).

  **test9.p :**

  > Test non fonctionnel : Le STEP est trop grand ce qui entraînera un i négatif et une boucle infini lors de l'exécution du fichier test.
